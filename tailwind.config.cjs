const config = {
  mode: 'jit',
  purge: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {},
    fontFamily: {
      'a-regular': ['a-regular'],
      'a-regular-italic': ['a-regular-italic'],
      'a-hairline': ['a-hairline'],
      'a-hairline-italic': ['a-hairline-italic'],
      'a-headline': ['a-headline'],
      'a-headline-italic': ['a-headline-italic'],
      'a-text-bold': ['a-text-bold'],
      'a-text-regular': ['a-text-regular'],
      'a-text-regular-italic': ['a-text-regular-italic'],
      'hk-grotesk-regular': ['hk-grotesk-regular'],
    },
  },
  plugins: [],
};

module.exports = config;
