import { text } from 'svelte/internal';
import { writable, get } from 'svelte/store';

/* export const imagesVisible = writable(false); */
/* export const fontEditor = writable(true); */
export const pausedStore = writable(Boolean);
export const textIsVisible = writable(false);
export const size = writable(0);
export const halfsize = writable(0);
export const activeTab = writable('Transcript');
export const font = writable('a-regular');
export const font2 = writable('lato-regular');
export const font3 = writable('a-regular');
export const ready = writable(false);
export const globalColour = writable('#f7e086');
export const networkError = writable({ status: false, message: '' });
export const playlist = writable([]);
export const playerKind = writable('player');

export const playvideo = () => {
  const status = get(pausedStore);
  console.log(status);
  const video = document.getElementsByTagName('video')[0];
  if (status) {
    video.play();
  } else if (!status) {
    video.pause();
  }
};

// fetch

export const cache = new Map();

export function getData(url) {
  const store = writable(new Promise(() => {}));
  if (cache.has(url)) {
    store.set(Promise.resolve(cache.get(url)));
  }

  const fetchData = async () => {
    try {
      const res = await fetch(url);
      const data = await res.json();
      cache.set(url, data);
      store.set(Promise.resolve(data));
      ready.set(true);
    } catch (error) {
      console.log('theres an error:', error);
      networkError.set({
        status: true,
        message:
          'Oop sorry, the media is not accesible right now :(... reload the website or try again later!',
      });
    }
  };
  fetchData();
  return store;
}
