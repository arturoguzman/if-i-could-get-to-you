# If I Could Get To You

### by ***Amaal Said*** 
... is an audio-visual project about interacting with the family archive. It features snippets of family footage, photography, conversations and poetry.  Weaving the memories, tales and images of these participants, the project brings them into conversation about what they’re holding on to and what they’re planning on passing down. It is about what connects us to loved ones who have entire lives abroad and only knowing them through fragments, questioning the information we’ve been given, and filling in the gaps. It came from Amaal’s experience of sitting with her grandfather in 2015 and coming away with no images and even more questions. When he passed away in January 2021, she found a voice note he had sent and  thought about how she could preserve his memory. If I Could Get To You is born out of a need to make the ghost-like figures of the family archive real, to give them a body. If there isn’t a documented story, the project asks participants to consider what it could have been.   

If I Could Get To You has been produced by Lighthouse and commissioned through Digital Democracies, a two-year programme led by Threshold Studios and supported by Arts Council England. Digital Democracies connects three UK-based commissioning festivals, Frequency International Festival of Digital Culture (produced by Threshold Studios), Freedom Festival (produced by Freedom Festival Arts Trust) and Brighton Digital Festival (directed by Lighthouse), along with a network of associate partners and collaborators across the UK.

#### Executive Producer: Alli Beddoes
#### Producer: Sian Habell-Aili
#### Assistant Producer: Lucie Rachel
#### Website and design: Arturo Guzmán Pérez
